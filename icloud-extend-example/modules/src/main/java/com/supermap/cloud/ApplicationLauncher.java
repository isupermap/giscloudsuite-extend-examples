package com.supermap.cloud;

import com.alibaba.fastjson.JSON;
import com.google.common.collect.Maps;
import com.netflix.appinfo.ApplicationInfoManager;
import com.netflix.appinfo.InstanceInfo;
import com.supermap.server.cloud.commontypes.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;

import java.util.Map;

public class ApplicationLauncher {

    public static void main(String[] args) {
        SpringApplication springApplication = new SpringApplication(new Class[]{Application.class});
        springApplication.addListeners(new ApplicationListener<ApplicationReadyEvent>() {

            @Override
            public void onApplicationEvent(ApplicationReadyEvent event) {
                ApplicationInfoManager applicationInfoManager = event.getApplicationContext().getBean(ApplicationInfoManager.class);
                ApplicationInfo applicationInfo = event.getApplicationContext().getBean(ApplicationInfo.class);
                long updateTime = System.currentTimeMillis();
                Map<String, String> appMetadata = Maps.newHashMap();
                String serviceprefix = getServiceprefix(applicationInfoManager);
                ApplicationServicesInfo applicationServicesInfo = new ApplicationServicesInfo();
                applicationServicesInfo.applicationInstanceId = applicationInfo.applicationInstanceId;
                applicationServicesInfo.applicationInstanceName = applicationInfo.applicationInstanceName;
                applicationServicesInfo.servicesInfo = Maps.newHashMap();
                ApplicationServiceInfo servicelistServiceInfo = new ApplicationServiceInfo();
                servicelistServiceInfo.appId = applicationServicesInfo.applicationInstanceId;
                servicelistServiceInfo.route = StringUtils.join("/iserver/services/hello");
                servicelistServiceInfo.serviceprefix = serviceprefix;
                servicelistServiceInfo.status = ServiceStatus.Created;
                servicelistServiceInfo.updateTime = updateTime;
                servicelistServiceInfo.appType = ApplicationType.Extend.typeId();
                applicationServicesInfo.servicesInfo.put(servicelistServiceInfo.route, servicelistServiceInfo);

                appMetadata.put("ApplicationInstanceName", applicationInfo.applicationInstanceName);
                appMetadata.put("ApplicationServicesInfo", JSON.toJSONString(applicationServicesInfo));
                appMetadata.put("UpdateTime", String.valueOf(updateTime));
                applicationInfoManager.registerAppMetadata(appMetadata);

//                LicenseInfo licInfo = LicenseTool.getLicenseInfo();
//                System.out.println(JSON.toJSONString(licInfo));
            }

            private String getServiceprefix(ApplicationInfoManager applicationInfoManager) {
                return StringUtils.join(applicationInfoManager.getInfo().isPortEnabled(InstanceInfo.PortType.SECURE) ? "https://" : "http://",
                        applicationInfoManager.getInfo().getHostName(), ":", applicationInfoManager.getInfo().getPort(), "/");
            }
        });
        springApplication.run(args);
    }
}
