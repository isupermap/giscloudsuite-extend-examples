package com.supermap.cloud;

import com.netflix.appinfo.ApplicationInfoManager;
import com.supermap.server.cloud.commontypes.ApplicationInfo;
import com.supermap.server.cloud.commontypes.ApplicationType;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.ServerProperties;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableAutoConfiguration
public class Application {

    @Bean
    public ApplicationInfo applicationInfo(ServerProperties serverProperties, ApplicationInfoManager applicationInfoManager) {
        ApplicationInfo result = new ApplicationInfo();
        result.isSecure = serverProperties != null && serverProperties.getSsl() != null && serverProperties.getSsl().isEnabled();
        result.port = serverProperties.getPort();
        result.applicationInstanceName = "ICNExtend-Default";
        result.applicationInstanceId = applicationInfoManager.getInfo().getId();
        return result;
    }

    @Bean
    public ApplicationType applicationType() {
        return ApplicationType.Extend;
    }
}
