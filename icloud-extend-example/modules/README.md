1 前提：
   1）icloud-extend-example依赖icloud-model，需提前将icloud-model模块install到maven仓库
   2）若需要使用iserver许可，可添加如下依赖，并提前将icloud-license模块install到maven仓库
      <dependency>
          <groupId>com.supermap.cloud</groupId>
          <artifactId>icloud-license</artifactId>
          <version>10.1.0-SNAPSHOT</version>
      </dependency>

2 准备环境
   1）执行mvn clean install 编译出icloud-extend-example.jar
   2）复制icloud-extend-example\modules\src\main\resources\application.properties文件到icloud-extend-example.jar同级目录
   3）修改application.properties配置项：
       i. server.port 服务端口
	   ii. eureka.client.serviceUrl.defaultZon 注册中心地址，如：http://localhost:8801/eureka/
	   iii. icloudnative.license.* 为许可相关参数，按需选择
   4）启动扩展示范服务：java -jar icloud-extend-example.jar
	   
3 访问服务
   1）扩展示范服务地址：
      http://localhost:{server.port}/iserver/services/hello
   2）监控端点：
      http://localhost:{server.port}/iserver/actuator
      http://localhost:{server.port}/iserver/actuator/health

